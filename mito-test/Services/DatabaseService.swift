//
//  DatabaseService.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 27..
//  Copyright © 2018. Edem. All rights reserved.
//

import RxRealm
import Realm
import RealmSwift
import RxSwift

enum DatabaseService {
    /**
     Get an observable Array<T> with changeset (deleted, inserted, updated).
     */
    static func changeSet<T: Object>(for type: T.Type) -> Observable<([T], RealmChangeset?)> {
        do {
            let realm = try Realm()
            let objects = realm.objects(T.self)
            return Observable.arrayWithChangeset(from: objects)
        } catch {
            return Observable.error(error)
        }
    }

    /**
     Get an observable realm collection of objects T.
     */
    static func collection<T: Object>(for type: T.Type) -> Observable<Results<T>> {
        do {
            let realm = try Realm()
            let objects = realm.objects(T.self)
            return Observable.collection(from: objects)
        } catch {
            return Observable.error(error)
        }
    }

    /**
     Get an observable Array<T> from the database!
     */
    static func array<T: Object>(for type: T.Type) -> Observable<[T]> {
        do {
            let realm = try Realm()
            let objects = realm.objects(T.self)
            return Observable.array(from: objects)
        } catch {
            return Observable.error(error)
        }
    }

    /**
     If a Realm object Schema changes, then you have to sync it in your app.
     If you don't do it, you will get "Swift Realm property '*' has been added ...."
    */
    static func refreshSchema() {
        let config = RLMRealmConfiguration.default()
        config.schemaVersion = 5
        config.migrationBlock = { (migration, oldSchemaVersion) in
            // nothing to do
        }
        RLMRealmConfiguration.setDefault(config)
    }
}
