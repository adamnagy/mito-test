//
//  ObservableType+Reachability.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 27..
//  Copyright © 2018. Edem. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

public extension ObservableType {
    /**
     Listen for reconnection till timeout.
    */
    public func retryOnConnect(timeout: TimeInterval) -> Observable<E> {
        return retryWhen { _ in
            return Reachability.rx.isConnected
                .timeout(timeout, scheduler: MainScheduler.asyncInstance)
        }
    }
}
