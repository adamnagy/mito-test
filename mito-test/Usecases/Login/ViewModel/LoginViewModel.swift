//
//  LoginViewModel.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 25..
//  Copyright © 2018. Edem. All rights reserved.
//

import RxSwift
import RxCocoa

// MARK: - Properties
struct LoginViewModel {
    let usernameFieldViewModel = InputField.ViewModel(hint: "Username ...", inputType: .plainText, textColor: .white)
    let passwordFieldViewModel = InputField.ViewModel(hint: "Password ...", inputType: .password, textColor: .white)
    let isLoading = BehaviorRelay<Bool>(value: false)
    let navigator = LoginNavigator()
    private let bag = DisposeBag()
}

// MARK: - Actions
extension LoginViewModel {
    func loginAction(username: String, password: String) -> Completable {
        isLoading.accept(true)
        let user = User(username: username, password: password)
        return AuthService.login(with: user)
            .asCompletable()
            .do(onDispose: { self.isLoading.accept(false) })
    }

    func successfulLoginAction() {
        navigator.navigateToShoppingList()
    }
}
