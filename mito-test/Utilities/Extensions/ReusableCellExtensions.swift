//
//  ReusableCellExtensions.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 27..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit

// If a UIView is a ReusableView, then have a reuseIdentifier
protocol ReusableView {}
extension ReusableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

// If a view loadable by a 'Nib', then have a 'nibName'
protocol NibLoadableView {}
extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
}

// Extend reusable views
extension UITableViewCell: ReusableView, NibLoadableView {}

// Register Cell
extension UITableView {
    public func register<T: UITableViewCell>(_: T.Type) {
        let nib = UINib(nibName: T.nibName, bundle: Bundle(for: T.self))
        register(nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
}
