//
//  UITextField+Extensions.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 29..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit

extension UITextField {
    func addDoneButton() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()

        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done",
                                         style: .done,
                                         target: self,
                                         action: #selector(self.endEditing(_:)))
        toolbar.items = [space, doneButton]
        inputAccessoryView = toolbar
    }
}
