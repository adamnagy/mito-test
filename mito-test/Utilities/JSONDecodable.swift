//
//  JSONDecodable.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 29..
//  Copyright © 2018. Edem. All rights reserved.
//

import Foundation

protocol JSONDecodable: Codable {}

extension JSONDecodable {
    static func decode(from data: Data) throws -> Self {
        return try JSONDecoder().decode(Self.self, from: data)
    }

    static func decodeArray(from data: Data) throws -> [Self] {
        return try JSONDecoder().decode([Self].self, from: data)
    }
}
