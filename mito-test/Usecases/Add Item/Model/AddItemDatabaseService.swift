//
//  AddItemDatabaseService.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 27..
//  Copyright © 2018. Edem. All rights reserved.
//

import RxSwift
import RealmSwift

struct AddItemDatabaseService {
    private static let bag = DisposeBag()

    /**
     Add new item to the shopping list!
    */
    static func addItem(name: String, quantity: Int) {
        // if network not reachable, then i just save item to the database as 'unsynced'
        guard NetworkListener.shared.isReachable else {
            let item = Item(name: name, quantity: String(quantity), synced: false)
            Observable.just(item).subscribe(Realm.rx.add()).disposed(by: bag)
            return
        }

        // otherwise i send it to the server and save the server's new state to the database
        API.addItemRequest(name: name, quantity: quantity)
            .flatMap { _ in ItemFetcher.fetchItems() }
            .subscribe(Realm.rx.add(update: true))
            .disposed(by: bag)
    }

    /**
     Add new item to the shopping list!
     */
    static func addMockItem(name: String, quantity: Int) {
        // if network not reachable, then i just save item to the database as 'unsynced'
        guard NetworkListener.shared.isReachable else {
            let item = Item(name: name, quantity: String(quantity), synced: false)
            Observable.just(item).subscribe(Realm.rx.add()).disposed(by: bag)
            return
        }

        // otherwise i send it to the server and save the server's new state to the database
        API.addItemRequest(name: name, quantity: quantity)
            .flatMap { _ in ItemFetcher.fetchItems() }
            .subscribe(Realm.rx.add(update: true))
            .disposed(by: bag)
    }
}
