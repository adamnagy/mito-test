//
//  AuthenticationTests.swift
//  mito-testTests
//
//  Created by Nagy Ádám on 2018. 07. 30..
//  Copyright © 2018. Edem. All rights reserved.
//
//swiftlint:disable all

@testable import mito_test
import RxSwift
import XCTest

class mito_testTests: XCTestCase {
    var bag: DisposeBag!
    
    override func setUp() {
        super.setUp()
        bag = DisposeBag()
    }
    
    override func tearDown() {
        bag = nil
        super.tearDown()
    }
    
    func testLoginSuccess() {
        let user = User(username: "mito", password: "teszt123")
        let promise = expectation(description: "expect successfull login")
        AuthService.login(with: user)
            .subscribe(onSuccess: { token in
                print(token)
                promise.fulfill()
            }, onError: { _ in
                XCTFail()
            }).disposed(by: bag)

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func testLoginFailure() {
        let user = User(username: "mitooooo", password: "teszt456")
        let promise = expectation(description: "expect failed login")
        AuthService.login(with: user)
            .subscribe(onSuccess: { _ in
                XCTFail()
            }, onError: { _ in
                promise.fulfill()
            }).disposed(by: bag)

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func testTokenAvailability() {
        TokenService.saveToken(TokenService.mockToken)
        XCTAssertTrue(TokenService.isTokenAvailable, "Token should be available from here.")
    }

    func testTokenDismissing() {
        AuthService.logout()
        XCTAssertFalse(TokenService.isTokenAvailable, "Token should be missing from here.")
    }
}
