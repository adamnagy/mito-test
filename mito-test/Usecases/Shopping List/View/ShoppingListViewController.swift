//
//  ShoppingListViewController.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 26..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

// MARK: - Properties
class ShoppingListViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    private var viewModel = ShoppingListViewModel()
    private let bag = DisposeBag()
}

// MARK: - LifeCycle
extension ShoppingListViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
        setupTableView()
    }
}

// MARK: - Setups
extension ShoppingListViewController {
    private func initialSetup() {
        title = "Shopping List"
        tableView.allowsSelection = false
        viewModel.navigator = ShoppingListNavigator(with: self)
    }

    private func setupTableView() {
        tableView.register(ShoppingListItemTableViewCell.self)
        tableView.rowHeight = 70.0

        viewModel.items.bind(to: tableView.rx.items(cellIdentifier: ShoppingListItemTableViewCell.reuseIdentifier,
                                                    cellType: ShoppingListItemTableViewCell.self)) { _, item, cell in
            cell.setup(with: item)
        }.disposed(by: bag)
    }
}

// MARK: - Actions
extension ShoppingListViewController {
    @IBAction private func logoutButtonDidTap(_ sender: UIBarButtonItem) {
        viewModel.logoutButtonTapped()
    }

    @IBAction private func addItemButtonDidTap(_ sender: UIBarButtonItem) {
        viewModel.addItemButtonTapped()
    }
}
