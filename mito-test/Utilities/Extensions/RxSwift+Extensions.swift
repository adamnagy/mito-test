//
//  RxSwift+Extensions.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 26..
//  Copyright © 2018. Edem. All rights reserved.
//

import Foundation
import RxSwift

extension ObservableType {

    /**
     Takes a sequence of optional elements and returns a sequence of non-optional elements, filtering out any nil values.
     - returns: An observable sequence of non-optional elements
     */

    public func unwrap<T>() -> Observable<T> where E == T? {
        return self.filter { $0 != nil }.map { $0! }
    }
}

extension PrimitiveSequenceType where Self.TraitType == RxSwift.SingleTrait {
    var asCompletable: Completable {
        return Completable.create { completable -> Disposable in
            return self.subscribe(onSuccess: { _ in
                completable(.completed)
            }, onError: { error in
                completable(.error(error))
            })
        }
    }
}

extension ObservableType {
    var asCompletable: Completable {
        return Completable.create { observer -> Disposable in
            return self.subscribe(onNext: { _ in
                observer(.completed)
            }, onError: { error in
                observer(.error(error))
            }, onCompleted: {
                observer(.completed)
            })
        }
    }
}
