//
//  UIWindow+Extensions.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 26..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit

extension UIWindow {

    // Change the Root View Controller (clear navigation stack)
    class func changeRootViewController(to vc: UIViewController, competion: (() -> Void)? = nil) {
        guard let window = UIApplication.shared.keyWindow, let root = window.rootViewController else { return }

        vc.view.frame = root.view.frame
        vc.view.layoutIfNeeded()

        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = vc
        }, completion: { _ in
            competion?()
        })
    }

    // Get the Top View Controller
    class var topViewController: UIViewController? {
        if var top = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = top.presentedViewController {
                top = presentedViewController
            }
            return top
        }
        return nil
    }

}
