//
//  Token.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 25..
//  Copyright © 2018. Edem. All rights reserved.
//

import Foundation

struct Token: JSONDecodable {
    let token: String
}
