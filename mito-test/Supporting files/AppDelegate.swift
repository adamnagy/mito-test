//
//  AppDelegate.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 25..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        NetworkListener.shared.start()
        DatabaseSyncer.shared.start()
        DatabaseService.refreshSchema()
        AppDelegateNavigator(window: window).navigateToRoot()
        return true
    }
}
