//
//  AddItemNavigator.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 26..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit

// MARK: - Properties
struct AddItemNavigator {
    let view: UIViewController?
}

// MARK: - Destinations
extension AddItemNavigator: Navigator {
    enum Destination {
        case back
    }
}

// MARK: - Actions
extension AddItemNavigator {
    func navigateBack() {
        view?.navigationController?.popViewController(animated: true)
    }
}

// MARK: - Utilities
extension AddItemNavigator {
    func navigate(to destination: Destination) {
        switch destination {
        case .back:
            navigateBack()
        }
    }
}
