//
//  Endpoints.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 27..
//  Copyright © 2018. Edem. All rights reserved.
//

import Alamofire

enum Endpoints {
    case login
    case fetchItems
    case postItem
}

extension Endpoints {
    var stringValue: String {
        switch self {
        case .fetchItems: return "/items"
        case .postItem: return "/item"
        case .login: return "/login"
        }
    }

    var httpMethod: HTTPMethod {
        switch self {
        case .fetchItems:
            return .get
        case .login, .postItem:
            return .post
        }
    }
}
