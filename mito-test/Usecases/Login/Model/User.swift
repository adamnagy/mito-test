//
//  User.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 25..
//  Copyright © 2018. Edem. All rights reserved.
//

import Foundation

struct User: Codable {
    let username: String
    let password: String
}
