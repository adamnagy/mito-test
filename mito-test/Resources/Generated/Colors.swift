// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSColor
  internal typealias Color = NSColor
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  internal typealias Color = UIColor
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable operator_usage_whitespace
internal extension Color {
  convenience init(rgbaValue: UInt32) {
    let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
    let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
    let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
    let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}
// swiftlint:enable operator_usage_whitespace

// swiftlint:disable identifier_name line_length type_body_length
internal struct Colors {
  internal let rgbaValue: UInt32
  internal var color: Color { return Color(named: self) }

  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#899797"></span>
  /// Alpha: 100% <br/> (0x899797ff)
  internal static let rdBluntGreen = Colors(rgbaValue: 0x899797ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ee6a46"></span>
  /// Alpha: 100% <br/> (0xee6a46ff)
  internal static let rdCarrot = Colors(rgbaValue: 0xee6a46ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#696d6f"></span>
  /// Alpha: 100% <br/> (0x696d6fff)
  internal static let rdDarkGrey = Colors(rgbaValue: 0x696d6fff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#d00606"></span>
  /// Alpha: 100% <br/> (0xd00606ff)
  internal static let rdDeepRed = Colors(rgbaValue: 0xd00606ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#9da1a7"></span>
  /// Alpha: 100% <br/> (0x9da1a7ff)
  internal static let rdGrey = Colors(rgbaValue: 0x9da1a7ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#3c4350"></span>
  /// Alpha: 100% <br/> (0x3c4350ff)
  internal static let rdGreyBlue = Colors(rgbaValue: 0x3c4350ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#585858"></span>
  /// Alpha: 100% <br/> (0x585858ff)
  internal static let rdLightBlack = Colors(rgbaValue: 0x585858ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#b3bdc2"></span>
  /// Alpha: 100% <br/> (0xb3bdc2ff)
  internal static let rdLightGrey = Colors(rgbaValue: 0xb3bdc2ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f03502"></span>
  /// Alpha: 100% <br/> (0xf03502ff)
  internal static let rdRed = Colors(rgbaValue: 0xf03502ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f7f7f8"></span>
  /// Alpha: 100% <br/> (0xf7f7f8ff)
  internal static let rdShellWhite = Colors(rgbaValue: 0xf7f7f8ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#646363"></span>
  /// Alpha: 100% <br/> (0x646363ff)
  internal static let rdSmokyBlack = Colors(rgbaValue: 0x646363ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#dbd9d9"></span>
  /// Alpha: 100% <br/> (0xdbd9d9ff)
  internal static let rdSmokyWhite = Colors(rgbaValue: 0xdbd9d9ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#06bebd"></span>
  /// Alpha: 100% <br/> (0x06bebdff)
  internal static let rdTurquise = Colors(rgbaValue: 0x06bebdff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ffffff"></span>
  /// Alpha: 100% <br/> (0xffffffff)
  internal static let rdWhite = Colors(rgbaValue: 0xffffffff)
}
// swiftlint:enable identifier_name line_length type_body_length

internal extension Color {
  convenience init(named color: Colors) {
    self.init(rgbaValue: color.rgbaValue)
  }
}
