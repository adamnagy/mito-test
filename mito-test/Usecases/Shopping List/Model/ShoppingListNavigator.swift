//
//  ShoppingListNavigator.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 26..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit

class ShoppingListNavigator {
    let rootView: UIViewController
    var navigationController: UINavigationController? {
        return rootView.navigationController
    }

    init(with view: UIViewController) {
        self.rootView = view
    }
}

extension ShoppingListNavigator {
    func navigateToLoginScreen() {
        AuthService.logout()
        let loginView = UIStoryboard(name: "Login", bundle: nil)
            .instantiateViewController(withIdentifier: String(describing: LoginViewController.self))
        UIWindow.changeRootViewController(to: loginView)
    }

    func navigateToAddItemScreen() {
        let addItemView = UIStoryboard(name: "AddItem", bundle: nil)
            .instantiateViewController(withIdentifier: String(describing: AddItemViewController.self))
        navigationController?.pushViewController(addItemView, animated: true)
    }
}
