//
//  AppDelegateNavigator.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 27..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit

// MARK: - Properties
struct AppDelegateNavigator {
    let window: UIWindow?
}

// MARK: - Destinations
extension AppDelegateNavigator: Navigator {
    enum Destination {
        case login
        case shoppingList
    }
}

// MARK: - Actions
extension AppDelegateNavigator {
    func navigateToRoot() {
        guard TokenService.isTokenAvailable else {
            navigate(to: .login)
            return
        }

        navigate(to: .shoppingList)
    }
}

// MARK: - Utilities
extension AppDelegateNavigator {
    func navigate(to destination: Destination) {
        window?.rootViewController = makeViewController(for: destination)
    }

    private func makeViewController(for destination: Destination) -> UIViewController {
        switch destination {
        case .login:
            return StoryboardScene.Login.loginViewController.instantiate()
        case .shoppingList:
            return StoryboardScene.ShoppingList.shoppingListNavigationController.instantiate()
        }
    }
}
