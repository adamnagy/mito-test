//
//  ItemFetcher.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 26..
//  Copyright © 2018. Edem. All rights reserved.
//

import Foundation
import RxSwift

struct ItemFetcher {
    static func fetchItems() -> Single<[Item]> {
        return API.itemsRequest()
            .map(Item.decodeArray)
            .asSingle()
    }

    static var storedItems: Observable<[Item]> {
        return DatabaseService.array(for: Item.self)
    }
}
