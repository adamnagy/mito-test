//
//  Logger.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 27..
//  Copyright © 2018. Edem. All rights reserved.
//

import Foundation

enum DebugEventType: String {
    case info = "[ℹ️]"
    case network = "[📡]"
    case debug = "[🐛]"
    case warning = "[⚠️]"
    case error = "[‼️]"
    case severe = "[🔥]"
    case memoryLeak = "[🚰]"
}

/// Provide informative log to console. Default event is `debug` and default isDate is `false`.
func debug(_ message: String?,
           fileName: String = #file,
           line: Int = #line,
           funcName: String = #function,
           event: DebugEventType = .debug) {

    let extractedFileName = fileName.components(separatedBy: "/").last ?? ""

    print("\(event.rawValue) - [\(message ?? "nil object")] - [📄: \(extractedFileName), 🌀: \(funcName), ✏️: \(line)]")
}

func debug(_ object: Any?,
           fileName: String = #file,
           line: Int = #line,
           funcName: String = #function,
           event: DebugEventType = .debug) {
    debug(object.debugDescription, event: event)
}
