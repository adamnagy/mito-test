//
//  ShoppingListTests.swift
//  mito-testTests
//
//  Created by Nagy Ádám on 2018. 07. 30..
//  Copyright © 2018. Edem. All rights reserved.
//
//swiftlint:disable all

@testable import mito_test
import RxSwift
import XCTest

class ShoppingListTests: XCTestCase {
    var bag: DisposeBag!
    
    override func setUp() {
        super.setUp()
        bag = DisposeBag()
    }
    
    override func tearDown() {
        bag = nil
        super.tearDown()
    }
    
    func testFetchShoppingList() {
        let promise = expectation(description: "expect to item fetcher can fetch items and won't produce error.")
        TokenService.saveToken(TokenService.mockToken)

        ItemFetcher.fetchItems()
            .subscribe(onSuccess: { _ in
                promise.fulfill()
            }, onError: { error in
                XCTFail("Can't fetch items because of ... \(error)")
            }).disposed(by: bag)

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func testFetchStoredFetchingList() {
        let promise = expectation(description: "expect to stored item fetcher can fetch items and won't produce error.")
        DatabaseService.array(for: Item.self)
            .take(1)
            .subscribe(onNext: { _ in
                promise.fulfill()
            }, onError: { error in
                XCTFail("Can't fetch items because of ... \(error)")
            }).disposed(by: bag)

        waitForExpectations(timeout: 5.0, handler: nil)
    }
}
