//
//  NetworkListener.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 27..
//  Copyright © 2018. Edem. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift
import RxRealm

final public class NetworkListener {
    private init() {}
    static let shared = NetworkListener()
    var isReachable: Bool = false

    lazy var reachability = Reachability()
    private let bag = DisposeBag()

    public func start() {
        do {
            try reachability?.startNotifier()

            reachability?.rx.isReachable
                .subscribe(onNext: { self.isReachable = $0 })
                .disposed(by: bag)
        } catch {
            debug("Network can not be listened. Message: \(error.localizedDescription)", event: .network)
        }
    }

    public func stop() {
        reachability?.stopNotifier()
    }

    deinit {
        stop()
    }
}
