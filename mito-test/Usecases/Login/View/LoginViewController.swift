//
//  LoginViewController.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 25..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

// MARK: - Properties
class LoginViewController: UIViewController {
    @IBOutlet weak var nameField: InputField!
    @IBOutlet weak var passwordField: InputField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!

    private let viewModel = LoginViewModel()
    private let bag = DisposeBag()
}

// MARK: - LifeCycle
extension LoginViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
}

// MARK: - Setups
extension LoginViewController {
    private func setupUI() {
        loginButton.layer.borderColor = UIColor.white.cgColor
        loginButton.layer.borderWidth = 3.0
        viewModel.isLoading.bind(to: loader.rx.isAnimating).disposed(by: bag)
        nameField.setup(with: viewModel.usernameFieldViewModel)
        passwordField.setup(with: viewModel.passwordFieldViewModel)
    }
}

// MARK: - Actions
extension LoginViewController {
    @IBAction func loginButtonDidTap(_ sender: UIButton) {
        viewModel.loginAction(username: nameField.value, password: passwordField.value)
            .subscribe(onCompleted: {
                self.viewModel.successfulLoginAction()
            }, onError: { _ in
                self.popErrorMessage(with: "Invalid credentials", message: "Check your username and password once again!")
            }).disposed(by: bag)
    }

    private func popErrorMessage(with title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
