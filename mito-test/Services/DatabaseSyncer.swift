//
//  DatabaseSyncer.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 29..
//  Copyright © 2018. Edem. All rights reserved.
//

import RxSwift
import RealmSwift
import RxRealm

// MARK: - LifeCycle
class DatabaseSyncer {
    private init() {}
    static let shared = DatabaseSyncer()
    private let bag = DisposeBag()

    func start() {
        observeNetworkConnection()
        observeNetworkType()
    }
}

// MARK: - Actions
extension DatabaseSyncer {
    /**
     Observe network reachability changes.
     If network became active, then i filter the unsynced items from the database and sync them!
     */
    private func observeNetworkConnection() {
        Reachability.rx.isReachable
            .distinctUntilChanged()         // it's no matter if network is changed from wifi to cellural.
            .do(onNext: logNetworkStatus)   // log connection status
            .filter { $0 == true }          // refresh database only when network became active
            .map { _ in }
            .subscribe(onNext: refreshItemsIfNeeded)
            .disposed(by: bag)
    }

    /**
     A convenient function which show us if network type changes.
     Possible values: [cellural, wifi, none]
     */
    private func observeNetworkType() {
        Reachability.rx.status
            .do(onNext: { status in debug("Network connection -> \(status)", event: .network) })
            .subscribe()
            .disposed(by: bag)
    }

    /**
     Log network connection type when it's changes.
     */
    private func logNetworkStatus(_ status: Bool) {
        debug("Network status is \(status ? "online" : "offline")", event: .network)
    }

    /**
     In offline mode, user add item to the database with {synced = false} attribute.
     When network became online from offline status, then this function refresh this offline elements from the
     database to the server!
     */
    private func refreshItemsIfNeeded() {
        let unsyncedItems = DatabaseService
            .array(for: Item.self)
            .take(1)
            .unsynced()

        unsyncedItems
            .map { $0.map { API.addItemRequest(name: $0.name, quantity: $0.quantityIntValue) } }
            .flatMap { Observable.zip($0) }
            .flatMap { _ in ItemFetcher.fetchItems() }
            .subscribe(Realm.rx.add(update: true))
            .disposed(by: bag)

        unsyncedItems
            .subscribe(Realm.rx.delete())
            .disposed(by: bag)
    }
}
