//
//  API.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 25..
//  Copyright © 2018. Edem. All rights reserved.
//

import RxSwift
import RxCocoa
import Alamofire
import RxAlamofire

struct API {
    // MARK: - Constants
    private static let baseURL = "http://dev.mito.hu/mobile-test"
    private static var authHeader: (String) -> [String: String] = {
        return ["Authorization": "Bearer \($0)"]
    }
}

// MARK: - Requests
extension API {
    /**
     Request login from the server for authentication.
    */
    static func loginRequest(with user: User) -> Observable<Data> {
        let parameters: [String: Any] = [
            "username": user.username,
            "password": user.password
        ]

        let endpoint = Endpoints.login
        let url = baseURL + endpoint.stringValue
        return RxAlamofire
            .request(endpoint.httpMethod, url, parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .do(onNext: { _ in
                debug("User trying to login with username: \(user.username), password: \(user.password)")
            })
            .responseData()
            .map { $0.1 }
    }

    /**
     Request shopping items from the server.
    */
    static func itemsRequest() -> Observable<Data> {
        guard let token = try? TokenService.retrieveToken() else {
            return Observable.error(APIError.missingToken)
        }

        let headers = authHeader(token.token)
        let endpoint = Endpoints.fetchItems
        let url = baseURL + endpoint.stringValue
        return RxAlamofire
            .request(endpoint.httpMethod, url, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .do(onNext: { _ in debug("User requested shopping list items.", event: .network) })
            .responseData()
            .map { $0.1 }
    }

    /**
     Request backend to add a new shopping list item with {name} and {quantity}.
    */
    static func addItemRequest(name: String, quantity: Int) -> Observable<DataResponse<Any>> {
        guard let token = try? TokenService.retrieveToken() else {
            return Observable.error(APIError.missingToken)
        }

        let parameters: [String: Any] = [
            "name": name,
            "quantity": quantity
        ]

        let headers = authHeader(token.token)
        let endpoint = Endpoints.postItem
        let url = baseURL + endpoint.stringValue
        return RxAlamofire
            .request(endpoint.httpMethod, url, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .do(onNext: { _ in debug("User requested to add a new item!", event: .network) })
            .responseJSON()
    }
}

// MARK: - Errors
extension API {
    enum APIError: Error {
        case wrongURL
        case missingToken
    }
}
