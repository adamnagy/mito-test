//
//  Item.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 26..
//  Copyright © 2018. Edem. All rights reserved.
//

import Foundation
import RealmSwift

/**
 Shopping Item.
 Can be persisted to database. If you change properties, then database scheme will be changed.
 Never forget to increment schema version in DatabaseService if you do that!
*/
class Item: Object, JSONDecodable {
    @objc dynamic var id: String = "0"
    @objc dynamic var name: String = ""
    @objc dynamic var quantity: String = "0"
    @objc dynamic var synced: Bool = true

    var quantityIntValue: Int {
        return Int(quantity) ?? 0
    }

    convenience init(name: String, quantity: String, synced: Bool = true) {
        self.init()

        self.id = "\(Date())"
        self.name = name
        self.quantity = quantity
        self.synced = synced
    }

    override static func primaryKey() -> String? {
        return "id"
    }

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case quantity
    }
}
