//
//  TokenService.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 29..
//  Copyright © 2018. Edem. All rights reserved.
//

import Foundation

// MARK: - Propeties
enum TokenService {
    private static let key = "token"

    static var isTokenAvailable: Bool {
        return (try? retrieveToken()) != nil
    }

    static let mockToken
        = Token(token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Im1pdG8ifQ.q9qRTXrdnDp0L5hG7yYZMGd4NsrGtKrSQwEQbyoM0zk")
}

// MARK: - Actions
extension TokenService {
    static func saveToken(_ token: Token) {
        UserDefaults.standard.set(token.token, forKey: key)
    }

    static func retrieveToken() throws -> Token {
        guard let tokenString = UserDefaults.standard.string(forKey: key) else {
            throw TokenServiceError.missingToken
        }
        return Token(token: tokenString)
    }

    static func deleteToken() {
        UserDefaults.standard.removeObject(forKey: key)
    }
}

// MARK: - Errors
extension TokenService {
    enum TokenServiceError: Error {
        case missingToken
    }
}
