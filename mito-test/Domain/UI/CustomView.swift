//
//  CustomView.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 25..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit

class CustomView: UIView {
    var contentView: UIView!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSelf()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSelf()
    }

    override public func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        initializeSelf()
    }

    private func initializeSelf() {
        initView()
    }

    func initView() {
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: nil)
        guard let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView else {
            fatalError("Can't instantiate view from nib!")
        }

        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        addSubview(view)
        layoutIfNeeded()
    }
}
