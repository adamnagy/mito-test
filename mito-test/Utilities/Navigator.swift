//
//  Navigator.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 27..
//  Copyright © 2018. Edem. All rights reserved.
//

protocol Navigator {
    associatedtype Destination

    func navigate(to destination: Destination)
}
