//
//  ShoppingListItemTableViewCell.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 26..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit

class ShoppingListItemTableViewCell: UITableViewCell {
    @IBOutlet weak private var itemNameLabel: UILabel!
    @IBOutlet weak private var itemQuantityTextLabel: UILabel!
    @IBOutlet weak private var itemQuantityNumberLabel: UILabel!

    var itemName: String {
        get { return itemNameLabel.text ?? "" }
        set { itemNameLabel.text = newValue }
    }

    var itemQuantity: Int {
        get { return Int(itemQuantityNumberLabel.text ?? "0") ?? 0 }
        set { itemQuantityNumberLabel.text = String(newValue) }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setup(with item: Item) {
        itemName = item.name.isEmpty ? "Missing name" : item.name
        itemQuantity = item.quantityIntValue
    }
}
