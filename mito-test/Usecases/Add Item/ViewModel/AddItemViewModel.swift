//
//  AddItemViewModel.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 26..
//  Copyright © 2018. Edem. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RealmSwift
import RxRealm

// MARK: - Properties
struct AddItemViewModel {
    let itemNameViewModel = InputField.ViewModel(hint: "Item name ...", inputType: .plainText, textColor: .black)
    let itemQuantityViewModel = InputField.ViewModel(hint: "Item quantity ...", inputType: .numeric, textColor: .black)
    var itemName = BehaviorRelay<String?>(value: nil)
    var itemQuantity = BehaviorRelay<Int?>(value: nil)
    var isSaveEnabled = BehaviorRelay<Bool>(value: false)
    var navigator: AddItemNavigator?
    private let bag = DisposeBag()
}

// MARK: - Actions
extension AddItemViewModel {
    func addItemAction() {
        guard let name = itemName.value, let quantity = itemQuantity.value else { return }
        AddItemDatabaseService.addItem(name: name, quantity: quantity)
    }

    func savedItemApprovedAction() {
        navigator?.navigateBack()
    }
}
