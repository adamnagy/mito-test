//
//  Item+Extensions.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 29..
//  Copyright © 2018. Edem. All rights reserved.
//

import RxSwift

/**
 Reserve the elements in Item array to show the last element if it's needed!
*/
extension ObservableType where E == [Item] {
    func reversed() -> Observable<[Item]> {
        return self.map { $0.reversed() }
    }
}

/**
 Filter only synced/unsynced items.
*/
extension ObservableType where E == [Item] {
    func unsynced() -> Observable<[Item]> {
        return self.map { $0.filter { $0.synced == false } }
    }

    func synced() -> Observable<[Item]> {
        return self.map { $0.filter { $0.synced == true } }
    }
}
