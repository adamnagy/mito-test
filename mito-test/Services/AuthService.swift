//
//  AuthService.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 25..
//  Copyright © 2018. Edem. All rights reserved.
//

import RxSwift

// MARK: - Actions
enum AuthService {
    static func login(with user: User) -> Single<Token> {
        return API.loginRequest(with: user)
            .map { try? Token.decode(from: $0) }
            .unwrap()
            .do(onNext: TokenService.saveToken)
            .asSingle()
    }

    static func logout() {
        TokenService.deleteToken()
    }
}
