//
//  LoginNavigator.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 30..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit

// MARK: - Properties
struct LoginNavigator {
}

// MARK: - Destinations
extension LoginNavigator: Navigator {
    enum Destination {
        case shoppingList
    }
}

// MARK: - Actions
extension LoginNavigator {
    func navigateToShoppingList() {
        navigate(to: .shoppingList)
    }
}

// MARK: - Utilities
extension LoginNavigator {
    func navigate(to destination: Destination) {
        UIWindow.changeRootViewController(to: makeViewController(for: destination))
    }

    private func makeViewController(for destination: Destination) -> UIViewController {
        switch destination {
        case .shoppingList:
            return StoryboardScene.ShoppingList.shoppingListNavigationController.instantiate()
        }
    }
}
