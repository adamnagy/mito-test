//
//  AddItemViewController.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 26..
//  Copyright © 2018. Edem. All rights reserved.
//

import UIKit
import RxSwift

class AddItemViewController: UIViewController {
    @IBOutlet weak var itemNameField: InputField!
    @IBOutlet weak var itemQuantityField: InputField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var saveButtonHeightConstraint: NSLayoutConstraint!

    private var viewModel = AddItemViewModel()
    private let bag = DisposeBag()
}

extension AddItemViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        setupUI()
    }
}

extension AddItemViewController {
    private func setup() {
        viewModel.navigator = AddItemNavigator(view: self)
    }

    private func setupUI() {
        title = "Add Item"
        itemNameField.setup(with: viewModel.itemNameViewModel)
        itemNameField.rx.text.bind(to: viewModel.itemName).disposed(by: bag)
        itemQuantityField.setup(with: viewModel.itemQuantityViewModel)
        itemQuantityField.rx.text.unwrap().map { Int($0) }.bind(to: viewModel.itemQuantity).disposed(by: bag)
    }
}
extension AddItemViewController {
    @IBAction func saveButtonDidTap(_ sender: UIButton) {
        viewModel.addItemAction()

        let alert = UIAlertController(title: "Item saved!", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            self.viewModel.savedItemApprovedAction()
        }))
        present(alert, animated: true, completion: nil)
    }
}
