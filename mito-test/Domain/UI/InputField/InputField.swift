//
//  InputField.swift
//  AllerGo
//
//  Created by Csaba Vidó on 2018. 07. 03..
//  Copyright © 2018. Csaba Vidó. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class InputField: CustomView {
    // MARK: - Properties
    @IBOutlet fileprivate weak var textField: UITextField!
    @IBOutlet private weak var hintLabel: UILabel!
    @IBInspectable var isDoneButtonActive: Bool = false {
        didSet {
            switch isDoneButtonActive {
            case true: textField.addDoneButton()
            case false: textField.inputAccessoryView = nil
            }
        }
    }

    var value: String {
        return textField.text ?? ""
    }

    // MARK: - LifeCycle
    override func initView() {
        super.initView()
        commonInit()
    }

    private func commonInit() {
        textField.delegate = self
    }
}

// MARK: - ViewModel {
extension InputField {
    struct ViewModel {
        let hint: String
        let inputType: InputType
        let textColor: UIColor
    }
}

// MARK: - Setups
extension InputField {
    func setup(with viewModel: ViewModel) {
        switch viewModel.inputType {
        case .password:
            textField.isSecureTextEntry = true
        case .email:
            textField.keyboardType = .emailAddress
        case .plainText:
            textField.keyboardType = .default
        case .numeric:
            textField.keyboardType = .numberPad
        }

        textField.returnKeyType = .done
        textField.textColor = viewModel.textColor

        hintLabel.text = viewModel.hint
        hintLabel.textColor = viewModel.textColor
    }
}

// MARK: - Delegates
extension InputField: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateHintUp()
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if value.count <= 0 {
            animateHintBack()
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - Animations
extension InputField {
    private func animateHintUp() {
        UIView.animate(withDuration: 0.2) {
            let scale = CGAffineTransform(scaleX: 0.6, y: 0.6)
            let translation = CGAffineTransform(translationX: 0.0, y: -30.0)
            self.hintLabel.transform = scale.concatenating(translation)
            self.hintLabel.layer.opacity = 0.3
        }
    }

    private func animateHintBack() {
        UIView.animate(withDuration: 0.2) {
            self.hintLabel.layer.opacity = 1.0
            self.hintLabel.transform = CGAffineTransform.identity
        }
    }
}

// MARK: - Input Type
extension InputField {
    enum InputType {
        case plainText
        case password
        case email
        case numeric
    }
}

// MARK: - Reactive extensions
extension Reactive where Base: InputField {
    var text: ControlProperty<String?> {
        return base.textField.rx.text
    }
}
