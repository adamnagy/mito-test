//
//  ShoppingListViewModel.swift
//  mito-test
//
//  Created by Nagy Ádám on 2018. 07. 26..
//  Copyright © 2018. Edem. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RealmSwift
import RxRealm

struct ShoppingListViewModel {
    // MARK: - Properties
    var items = BehaviorRelay<[Item]>(value: [])
    private let bag = DisposeBag()
    var navigator: ShoppingListNavigator?

    // MARK: - LifeCycle
    init() {
        // Display ALWAYS the CURRENT context from the database!
        ItemFetcher.storedItems
            .reversed()
            .bind(to: items)
            .disposed(by: bag)

        // Fetch items and add them to the database if it's needed!
        ItemFetcher.fetchItems()
            .asObservable()
            .subscribe(Realm.rx.add(update: true))
            .disposed(by: bag)
    }
}

// MARK: - Actions
extension ShoppingListViewModel {
    func addItemButtonTapped() {
        navigator?.navigateToAddItemScreen()
    }

    func logoutButtonTapped() {
        navigator?.navigateToLoginScreen()
    }
}
